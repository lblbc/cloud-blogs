/**
 * 厦门大学计算机专业 | 华为开发专家(HDE)
 * 专注《零基础学编程系列》  http://lblbc.cn/blog
 * 包含：鸿蒙 | Java | 安卓 | 前端 | Flutter | iOS | 小程序
 * 公众号：蓝不蓝编程
 */
import { Context } from '@kit.AbilityKit';
import Preferences from './Preferences';

export class LoginManager {
  private static _instance: LoginManager;
  private context: Context;
  private token: string;

  private constructor() {
  }

  public static getInstance(): LoginManager {
    if (!LoginManager._instance) {
      LoginManager._instance = new LoginManager();
    }
    return LoginManager._instance;
  }

  public init(context: Context): void {
    this.context = context;
    this.token = Preferences.get(context).getToken()
  }

  public getToken(): string {
    return this.token;
  }

  public saveToken(token: string) {
    this.token = token;
    Preferences.get(this.context).saveToken(token);
  }

  public isLogin(): boolean {
    return !!this.token;
  }
}
