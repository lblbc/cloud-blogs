/**
 * 厦门大学计算机专业 | 华为开发专家(HDE)
 * 专注《零基础学编程系列》  http://lblbc.cn/blog
 * 包含：鸿蒙 | Java | 安卓 | 前端 | Flutter | iOS | 小程序
 * 公众号：蓝不蓝编程
 */
import axios, { AxiosError, AxiosInstance, InternalAxiosRequestConfig } from '@ohos/axios';
import { LoginManager } from '../utils/LoginManager';

const axiosInstance: AxiosInstance = axios.create({
  baseURL: 'https://lblbc.cn/',
});

axiosInstance.interceptors.request.use(
  (config: InternalAxiosRequestConfig) => {
    const token = LoginManager.getInstance().getToken();
    if (token) {
      config.headers['Authorization'] = token;
    }
    return config;
  },
  (error: AxiosError) => {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

export default axiosInstance;